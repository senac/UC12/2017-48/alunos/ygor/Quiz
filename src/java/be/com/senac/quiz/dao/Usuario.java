package be.com.senac.quiz.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sala304b
 */
class Usuario {
   private  int id;

    public Usuario(int id, String nome, String senha, String apelido) {
        this.id = id;
        this.nome = nome;
        this.senha = senha;
        this.apelido = apelido;
    }
    private String nome;
    private String senha;
   private String apelido;
    
    

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    
    
}
