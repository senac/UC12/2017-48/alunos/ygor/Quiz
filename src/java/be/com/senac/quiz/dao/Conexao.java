
package be.com.senac.quiz.dao;

import com.mysql.jdbc.Connection;
import com.sun.tools.xjc.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.text.normalizer.UBiDiProps;

public class Conexao {
 private static final  String URL = "jdbc:mysql://localhost:3306/Quiz" ;
 private static final  String USER = "root";
 private static final  String PASSWORD = "123456";
 private static final  String DRIVER = "com.mysql.jdbc.Driver";


public static Connection geConnection() {
    Connection connection = null;
    
     try {
         Class.forName(DRIVER);
         System.out.println("Carregando...");
         
    connection  = (Connection) DriverManager.getConnection(URL,USER,PASSWORD); 
         System.out.println("Conectado");
     
     } catch (ClassNotFoundException ex) {
         System.out.println("Erro driver..."); 
     }catch(SQLException ex){
         ex.printStackTrace();
         System.out.println("Erro banco...");    
     }
    
    
   return connection; 
}
public static void select() {
try{
Connection connection = Conexao.geConnection();

String query = "select * from usuario;";

    Statement statment = connection.createStatement();
    
    ResultSet rs = statment.executeQuery(query);
    List<Usuario>lista  =  new ArrayList<>();
    
    while (rs.next()){
    int id = rs.getInt("id");
    String nome = rs.getString("nome");
    String senha = rs.getString("senha");
    String apelido = rs.getString("apelido");
    
    Usuario usuario = new Usuario(id, nome, apelido, senha);
        lista.add(usuario);
    }
connection.close();
}    catch (SQLException ex) {
     System.out.println("Erro query...");
     }
}


    public static void main(String[] args) {
       Conexao.geConnection();
       
    }
}
