package be.com.senac.quiz.dao;





import java.util.List;


public interface DAO<T> {
    List<T> getLista()  ; 

    void inserir(T objeto) ;

    void atualizar(T objeto) ;

    void delete(int id);

    T getPorId(int id) ;
    
}
